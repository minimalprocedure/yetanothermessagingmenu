/*
#-------------------------------------------------------------------------------
# Copyright (c) 2012 Massimo Maria Ghisalberti {http://pragmas.org}.
# All rights reserved. This program and the accompanying materials
# are made available under the terms of the GNU Public License v2.0
# which accompanies this distribution, and is available at
# http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
# 
# Contributors:
#			thanks to https://extensions.gnome.org/accounts/profile/screenfreeze for idea
#     Massimo Maria Ghisalberti {http://pragmas.org}
#-------------------------------------------------------------------------------
*/

const Gdk = imports.gi.Gdk;
const GLib = imports.gi.GLib;
const Lang = imports.lang;
const Shell = imports.gi.Shell;
const St = imports.gi.St;
const Gio = imports.gi.Gio;

const Main = imports.ui.main;
const PanelMenu = imports.ui.panelMenu;
const PopupMenu = imports.ui.popupMenu;
const Panel = imports.ui.panel;

const Gettext = imports.gettext.domain('gnome-shell-extensions');
const _ = Gettext.gettext;

const ExtensionUtils = imports.misc.extensionUtils;
const Me = ExtensionUtils.getCurrentExtension();
const Convenience = Me.imports.convenience;
const Configuration = Me.imports.configuration;

const ApplicationMenuItem = new Lang.Class({
	Name : 'ApplicationMenu.ApplicationMenuItem',
	Extends : PopupMenu.PopupBaseMenuItem,

	_init : function(app) {
		this.parent();
		this._app = app;

		this.label = new St.Label({
			text : app.get_name(),
			style_class : 'application-label'
		});
		this.addActor(this.label);

		this._icon = app.create_icon_texture(Configuration.ICON_SIZE);
		this.addActor(this._icon, {
			align : St.Align.END,
			span : -1
		});

	},

	activate : function(event) {
		this._app.activate_full(-1, event.get_time());
		this.parent(event);
	}

});

const ApplicationMenu = new Lang.Class({
	Name : 'ApplicationMenu.ApplicationMenu',
	Extends : PanelMenu.SystemStatusButton,

	_init : function() {
		this.groupMenus = [];
		this.parent(Configuration.MENU_ICON);

		this.actor.connect('notify::hover', Lang.bind(this, this._OnHoverButton));
		this.actor.connect('button-press-event', Lang.bind(this, this._OnPressButton));

		this._buildMenu();
	},

	_OnHoverButton : function() {
		if(this.menu.isOpen) {
			for(var i = 0; i < this.groupMenus.length; i++) {
				var groupmenu = this.groupMenus[i];
					if (groupmenu.isOpened) {
						groupmenu.activate();
					}
				}
			}
	},

	_OnPressButton : function(actor, event){
			for(var i = 0; i < this.groupMenus.length; i++) {
				var groupmenu = this.groupMenus[i];
					if (groupmenu.isOpened) {
						groupmenu.activate();
					}
				}
	},

	_buildMenu : function() {

		let appsys = Shell.AppSystem.get_default();

		for(var i = 0; i < Configuration.Applications.length; i++) {

			let Applications = Configuration.Applications[i]
			var currentMenu = this.menu;

			for(application in Applications){
				if (application == Configuration.GROUP_PLACEHOLDER) {

					if(Applications[application].separator) {
						currentMenu.addMenuItem(new PopupMenu.PopupSeparatorMenuItem());
					}

					let groupmenu = new PopupMenu.PopupSubMenuMenuItem(_(Applications[application].caption));
					groupmenu.isOpened = false;

					groupmenu.actor.connect('button-release-event', Lang.bind(this, function (actor, event) {
						if (groupmenu.isOpened) {
							groupmenu.isOpened = false;
						}	else {
							groupmenu.isOpened = true;
						}
					}));

					this.groupMenus.push(groupmenu);
					currentMenu.addMenuItem(groupmenu);
					currentMenu = groupmenu.menu;

				} else {

					let app =  appsys.lookup_app(application + '.desktop');
					if (app != null) {
						currentMenu.addMenuItem(new ApplicationMenuItem(app));

						if(Applications[application].components != undefined){
							for(var c in Applications[application].components){
								var component = Applications[application].components[c];
								var menuitem = new PopupMenu.PopupMenuItem(Configuration.INDENT + _(component.caption));
								menuitem.connect('activate', Lang.bind(this,
										function(){ Main.Util.trySpawnCommandLine(component.action);}));
								currentMenu.addMenuItem(menuitem);
							}
						}

					}
				}
			}
		}
	},

	destroy : function() {
		this.parent();
	}

});

function init(extensionMeta) {
	Convenience.initTranslations();
	let theme = imports.gi.Gtk.IconTheme.get_default();
	theme.append_search_path(extensionMeta.path + "/icons");
}

let _indicator;

function enable() {
	_indicator = new ApplicationMenu;
	Main.panel.addToStatusArea('application-menu', _indicator, 1);
}

function disable() {
	_indicator.destroy();
}
