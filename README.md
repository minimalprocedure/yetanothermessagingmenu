{
  "_generated": "Generated by SweetTooth, do not edit",
  "description": "Yet Another Messaging Menu for the Gnome Shell.",
  "gettext-domain": "gnome-shell-extensions",
  "name": "Yet Another Messaging Menu",
  "shell-version": [
    "3.6"
  ],
  "url": "https://github.com/minimalprocedure/yetanothermessagingmenu.git",
  "uuid": "yetanothermessagingmenu@pragmas.org",
  "version": 2
}
