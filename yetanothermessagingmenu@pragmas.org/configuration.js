/*
#-------------------------------------------------------------------------------
# Copyright (c) 2012 Massimo Maria Ghisalberti {http://pragmas.org}.
# All rights reserved. This program and the accompanying materials
# are made available under the terms of the GNU Public License v2.0
# which accompanies this distribution, and is available at
# http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
# 
# Contributors:
#     Massimo Maria Ghisalberti {http://pragmas.org} - initial API and implementation
#-------------------------------------------------------------------------------
*/

const ICON_SIZE = 22;
const INDENT = "  ";

const GROUP_PLACEHOLDER = "----";
const MENU_ICON = 'application-symbolic';

const Applications = [
		{
			"evolution" : {
				"components" : {
					"mail" : {
						"caption" : "Compose New Message",
						"action" : "evolution -c mail mailto:"
					},
					"calendar" : {
						"caption" : "Calendar",
						"action" : "evolution -c calendar"
					},
					"contacts" : {
						"caption" : "Contacts",
						"action" : "evolution -c contacts"
					},
					"task" : {
						"caption" : "Tasks",
						"action" : "evolution -c tasks"
					},
					"memos" : {
						"caption" : "Memos",
						"action" : "evolution -c memos"
					}
				}
			}
		},
		{
			"----" : {
				"caption" : "Email applications",
				"separator" : true
			},
			"evolution" : {
				"components" : {
					"mail" : {
						"caption" : "Compose New Message",
						"action" : "evolution -c mail mailto:"
					},
					"calendar" : {
						"caption" : "Calendar",
						"action" : "evolution -c calendar"
					},
					"contacts" : {
						"caption" : "Contacts",
						"action" : "evolution -c contacts"
					},
					"task" : {
						"caption" : "Tasks",
						"action" : "evolution -c tasks"
					},
					"memos" : {
						"caption" : "Memos",
						"action" : "evolution -c memos"
					}
				}
			},
			"thunderbird" : {
				"components" : {
					"mail" : {
						"caption" : "Compose New Message",
						"action" : "thunderbird -compose"
					},
					"contacts" : {
						"caption" : "Contacts",
						"action" : "thunderbird -addressbook"
					}
				}
			},
			"claws-mail" : {
				"components" : {
					"mail" : {
						"caption" : "Compose New Message",
						"action" : "claws-mail --compose"
					}
				}
			},
			"KMail2" : {
				"components" : {
					"mail" : {
						"caption" : "Compose New Message",
						"action" : "kmail -compose"
					}
				}
			},
			"postler" : {},
			"gnome-gmail" : {},
			"geary" : {},

		},
		{
			"----" : {
				"caption" : "Chat applications",
				"separator" : true
			},
			"skype" : {},
			"pidgin" : {},
			"empathy" : {},
			"xchat" : {},
			"kmess" : {},
			"gajim" : {},
			"emesene" : {},
			"qutim" : {},
			"amsn" : {},
			"openfetion" : {},
		},
		{
			"----" : {
				"caption" : "Microblog applications",
				"separator" : true
			},
			"gwibber" : {},
			"pino" : {},
			"hotot" : {},
			"turpial" : {},
			"twitux" : {},
			"gtwitter" : {},
			"qwit" : {},
			"mitter" : {},
			"polly" : {}
		}
];
